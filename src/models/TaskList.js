export default class TaskList{
    constructor(taskArray){
        taskArray===undefined ? this.taskArray =[]:this.taskArray = taskArray
    }

    get tasks(){
        return this.taskArray
    }

    addTask(task){
        this.taskArray.push(task)
    }
}