Class Week extends TaskList{
    constructor(startDate, taskList){
        super(taskList)
        this.startDate = startDate
    }

    getStartDate(){
        return this.startDate
    }
}